package com.microservicios.service;

import java.util.Optional;

import com.microservicios.entity.Materia;

public interface MateriaService {
	
	public Optional<Materia> getByNombre(String nombre);
	public Optional<Materia> getByIdMateria(Integer idMateria);
}
