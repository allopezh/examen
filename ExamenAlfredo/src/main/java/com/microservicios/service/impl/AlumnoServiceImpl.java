package com.microservicios.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.microservicios.dto.DtoAlumno;
import com.microservicios.entity.Alumno;
import com.microservicios.entity.Materia;
import com.microservicios.repository.AlumnoRepository;
import com.microservicios.repository.MateriaRepository;
import com.microservicios.service.AlumnoService;
import com.microservicios.service.MateriaService;

@Service
public class AlumnoServiceImpl implements AlumnoService{
	
	@Autowired AlumnoRepository alumnoRepository;
	@Autowired MateriaRepository materiaRepository;
	@Autowired MateriaService materiaService;

	@Override
	public void save(DtoAlumno dtoAlumno) {
		Alumno alumno = new Alumno();
		Set<Materia> materiaSet = new HashSet<>();
		
		alumno.setNombre(dtoAlumno.getNombre());
		alumno.setApellidoPaterno(dtoAlumno.getApellidoPaterno());
		alumno.setApellidoMaterno(dtoAlumno.getApellidoMaterno());
		alumno.setMatricula(dtoAlumno.getMatricula());
		
		for (int i = 0; i < dtoAlumno.getDtoMateria().size(); i++) {
			materiaSet.add(materiaService.getByIdMateria(dtoAlumno.getDtoMateria().get(i).getIdMateria()).get());
		}
		
		alumno.setMateria(materiaSet);
		alumnoRepository.saveAndFlush(alumno);
	}

	@Override
	public List<Alumno> getAlumnos() {
		return alumnoRepository.findAll();
	}

	@Override
	public List<Object[]> getCalificacionMateria() {	
		List<Object[]> alumnoList = alumnoRepository.getCalificacionesAlumnoMateria();
	        return  alumnoList;
	}

}
