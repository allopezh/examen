package com.microservicios.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.microservicios.entity.Materia;
import com.microservicios.repository.MateriaRepository;
import com.microservicios.service.MateriaService;

@Service
public class MateriaServiceImpl implements MateriaService{


	@Autowired MateriaRepository materiaRepository;

	public Optional<Materia> getByNombre(String nombre){
		return materiaRepository.findByNombre(nombre);
	}

	@Override
	public Optional<Materia> getByIdMateria(Integer idMateria) {
		return materiaRepository.findById(idMateria);
	}

	
}
