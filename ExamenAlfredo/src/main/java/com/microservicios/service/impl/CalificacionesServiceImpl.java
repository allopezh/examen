package com.microservicios.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.microservicios.dto.DtoCalificar;
import com.microservicios.entity.Alumno;
import com.microservicios.entity.Calificaciones;
import com.microservicios.entity.Materia;
import com.microservicios.repository.AlumnoRepository;
import com.microservicios.repository.CalificacionRepository;
import com.microservicios.repository.MateriaRepository;
import com.microservicios.service.CalificacionesService;

@Service
public class CalificacionesServiceImpl implements CalificacionesService{

	@Autowired AlumnoRepository alumnoRepository;
	@Autowired MateriaRepository materiaRepository;
	@Autowired CalificacionRepository calificacionRepository;
	
	@Override
	public void save(DtoCalificar dtoCalificar) {
		Calificaciones calificaciones = new Calificaciones();
		Optional<Alumno> alumno = alumnoRepository.findByMatricula(dtoCalificar.getMatricula());
		Optional<Materia> materia = materiaRepository.findById(dtoCalificar.getIdMateria());
		
		calificaciones.setCalificacion(dtoCalificar.getCalificacion());
		calificaciones.setAlumno(alumno.get());
		calificaciones.setMateria(materia.get());
		
		calificacionRepository.saveAndFlush(calificaciones);
	}

	@Override
	public void update(DtoCalificar dtoCalificar) {
		
		Optional<Alumno> alumno = alumnoRepository.findByMatricula(dtoCalificar.getMatricula());
		Calificaciones calificaciones = getOne(alumno.get().getIdAlumno(), dtoCalificar.getIdMateria()).get();
		
		calificaciones.setCalificacion(dtoCalificar.getCalificacion());
		
		calificacionRepository.saveAndFlush(calificaciones);
	}

	@Override
	public Optional<Calificaciones> getOne(Integer id_alumno, Integer id_materia) {
		 Optional<Calificaciones> calificaciones_optional = calificacionRepository.getCalificaciones(id_alumno, id_materia);
		 return calificaciones_optional;
	}

	@Override
	public void delete(Integer id_materia, String matricula) {
		Optional<Alumno> alumno = alumnoRepository.findByMatricula(matricula);
		Calificaciones calificaciones = getOne(alumno.get().getIdAlumno(), id_materia).get();
		calificacionRepository.deleteById(calificaciones.getIdCalificacion());
	}


}
