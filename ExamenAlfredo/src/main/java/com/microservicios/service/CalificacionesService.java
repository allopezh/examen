package com.microservicios.service;

import java.util.Optional;

import com.microservicios.dto.DtoCalificar;
import com.microservicios.entity.Calificaciones;

public interface CalificacionesService {

	public void save(DtoCalificar dtoCalificar);
	public void update(DtoCalificar dtoCalificar);
	public Optional<Calificaciones> getOne(Integer id_alumno, Integer id_materia);
	public void delete(Integer id_materia, String matricula);
}
