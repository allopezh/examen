package com.microservicios.service;

import java.util.List;

import com.microservicios.dto.DtoAlumno;
import com.microservicios.entity.Alumno;

public interface AlumnoService {

	public void  save(DtoAlumno dtoAlumno);
	public List<Alumno> getAlumnos();
    public List<Object[]> getCalificacionMateria();
}
