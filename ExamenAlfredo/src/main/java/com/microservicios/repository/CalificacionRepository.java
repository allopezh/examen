package com.microservicios.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.microservicios.entity.Calificaciones;

@Repository
public interface CalificacionRepository extends JpaRepository<Calificaciones, Integer> {

	@Query(value = "SELECT * FROM calificaciones c WHERE c.id_alumno = :id_alumno and c.id_materia = :id_materia",  nativeQuery = true)
	Optional<Calificaciones> getCalificaciones(@Param("id_alumno")Integer id_alumno, 
			                                   @Param("id_materia") Integer id_materia);

}
