package com.microservicios.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.microservicios.entity.Alumno;
import com.microservicios.entity.Calificaciones;

@Repository
public interface AlumnoRepository extends JpaRepository<Alumno, Integer>{

	Optional<Alumno> findByMatricula(String matricula);
	
	@Query(value = "SELECT distinct a.nombre, a.apellido_paterno, a.apellido_materno, a.matricula, m.nombre as materia , c.calificacion FROM alumno as a \r\n" + 
			" inner join alumno_materia as am on am.alumno_id = a.id_alumno\r\n" + 
			" inner join materia as m on m.id_materia = am.materia_id\r\n" + 
			" inner join calificaciones as c on c.id_alumno = a.id_alumno",  nativeQuery = true)
	List<Object[]> getCalificacionesAlumnoMateria();
}
