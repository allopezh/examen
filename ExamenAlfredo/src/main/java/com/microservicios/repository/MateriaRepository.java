package com.microservicios.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.microservicios.entity.Materia;

@Repository
public interface MateriaRepository extends JpaRepository<Materia, Integer>{

	Optional<Materia> findByNombre(String nombre);
	Optional<Materia> findById(Integer idMateria);
}
