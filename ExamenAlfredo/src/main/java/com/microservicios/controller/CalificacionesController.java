package com.microservicios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.microservicios.dto.DtoCalificar;
import com.microservicios.service.CalificacionesService;

@RestController
@RequestMapping("/calificar")
public class CalificacionesController {
	
	@Autowired CalificacionesService calificacionesService;

	@PostMapping
    public ResponseEntity<?> create(@RequestBody DtoCalificar dtoCalificar){
		calificacionesService.save(dtoCalificar);
		return new ResponseEntity("calificacion asignada", HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<?> update(@RequestBody DtoCalificar dtoCalificar){
		calificacionesService.update(dtoCalificar);
		return new ResponseEntity("calificacion Actualizada", HttpStatus.OK);
	}
	
	@DeleteMapping
	public ResponseEntity<?> delete(@RequestParam Integer id_materia, @RequestParam String matricula){
		calificacionesService.delete(id_materia, matricula);
		return new ResponseEntity("calificacion Eliminada", HttpStatus.OK);
	}
}
