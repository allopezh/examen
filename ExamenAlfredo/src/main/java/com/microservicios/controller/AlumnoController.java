package com.microservicios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microservicios.dto.DtoAlumno;
import com.microservicios.service.AlumnoService;
import com.microservicios.service.MateriaService;

@RestController
@RequestMapping("/alumno")
public class AlumnoController {
	
	@Autowired AlumnoService alumnoService;
	@Autowired MateriaService materiaService;

	@PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody DtoAlumno dtoAlumno){
		alumnoService.save(dtoAlumno);
		 return new ResponseEntity("alumno creado", HttpStatus.OK);
	}
	
	@GetMapping()
	public  ResponseEntity<?> getAlumnos(){
		 return new ResponseEntity(alumnoService.getAlumnos(), HttpStatus.OK);
	}
	
	@GetMapping("/calificacionMateria")
	public  ResponseEntity<?> calificacionMateria(){
		 return new ResponseEntity(alumnoService.getCalificacionMateria(), HttpStatus.OK);
	}
	
}
