package com.microservicios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamenAlfredoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenAlfredoApplication.class, args);
	}

}
