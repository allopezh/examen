package com.microservicios.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Table
@Entity
@Getter
@Setter
public class Calificaciones implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCalificacion;
	
	private Integer calificacion;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idAlumno")
	private Alumno alumno;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idMateria")
	private Materia materia;
}
