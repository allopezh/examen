package com.microservicios.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DtoMateria {

	private Integer idMateria;
}
