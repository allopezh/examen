package com.microservicios.dto;

import java.util.List;

import com.microservicios.entity.Materia;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DtoAlumno {

	private String nombre;

	private String apellidoPaterno;

	private String apellidoMaterno;
	
	private String matricula;
	
	private List<DtoMateria> dtoMateria;
}
