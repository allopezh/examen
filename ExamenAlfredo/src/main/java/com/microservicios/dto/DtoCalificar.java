package com.microservicios.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DtoCalificar {

	private String matricula;
	private Integer calificacion;
	private Integer idMateria;
}
