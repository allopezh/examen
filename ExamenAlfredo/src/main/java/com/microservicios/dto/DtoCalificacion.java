package com.microservicios.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DtoCalificacion {

	private Integer calificacion;
}
